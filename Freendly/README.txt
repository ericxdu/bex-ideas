Mr. Friendly, a GZDoom mod by J.P.LeBreton, contains object description 
strings that do not really match up with Freedoom's object sprites. 
However, the mod offers a way to change the strings by putting special 
entries in a DeHackEd lump. This mod is an attempt to reconcile the
strings with the latest version of Freedoom (0.12.1) and includes the
sprites in the WAD for good measure.
