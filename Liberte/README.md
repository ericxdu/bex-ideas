This is a concept deathmatch map taking place on a starship in the
Freedoom/HD universe. It should really be more like ["Solar Flare" by
John Krutke](https://www.doomworld.com/idgames/levels/doom2/deathmatch/d-f/flare-2).
Also see this [idgames link](idgames://2418). Solar Flare is a
spaceship map designed for deathmatch play.

This ship is called O.S.S. Liberté. Other concepts involve using this
ship as a base for a deathmatch map where players step on teleporters
to get to and from a planetside combat area, or using the docking
hatches to access another ship or structure, or both.
