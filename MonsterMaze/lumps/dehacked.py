#!/usr/bin/env python3
from __future__ import print_function

from deh9000 import *

miscdata.initial_bullets = 0

weaponinfo[wp_fist].clear_states()
weaponinfo[wp_fist].update(states.parse("""
	Ready:
		PUNG A 1 A_WeaponReady
		Loop
	Deselect:
		PUNG A 1 A_Light0
		PUNG A 1 A_Lower
		Loop
	Select:
		PUNG A 1 A_Raise
		PUNG A 1 A_Light1
		Loop
	Fire:
		PUNG B 4
		PUNG C 4 A_Light2
		PUNG D 5
		PUNG C 4
		PUNG B 5 A_Light1
		Goto Ready
"""))

strings.HUSTR_1 = "Level 1: Caves"
strings["RUNNIN"] = "TIMEWO"

dehfile.save("dehacked.lmp")

state_ids = dehfile.free_states()
if state_ids:
	print("Still have %d states remaining" % len(state_ids))
