[flats]
flat1
flat4
tileset1
tileset2
tileset3
tileset4

[levels]
map01

[lumps]
dehacked

[musics]
d_runnin
d_timewo

[patches]
tileset5
tileset6
w31_1

[sprites]
csawa0
froga0
frogb0
frogc0
frogd0
froge0
frogf0
frogg0
frogh0
frogi0
frogj0
