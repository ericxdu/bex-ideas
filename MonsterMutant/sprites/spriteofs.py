import os

from PIL import Image

for filename in os.listdir():
    name, ext = os.path.splitext(filename)
    if ext in (".png", ".gif", ".tif", ".ppm"):
        image = Image.open(filename)
        (width, height) = image.size
        print(name + "\t" + str(int( width / 2 )) + "\t" + str( height - 5 ))
        rgba = image.convert("RGBA")
        datas = rgba.getdata()
        new_data = []
        for item in datas:
            if item == (0, 255, 255, 255):
                new_data.append((0, 0, 0, 0))
            else:
                new_data.append(item)
        rgba.putdata(new_data)
        rgba.save(filename, "PNG")
