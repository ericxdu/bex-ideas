from sprite_names import *

number = 15
name = "Bug"
initial = (
  dict(spr=HEAD, sub=A, tics=15, act='Look'),
  dict(spr=HEAD, sub=B, tics=15, act='Look', goto=-1)
)
moving = (
  dict(spr=HEAD, sub=A, tics=3, act='Chase', tag='see'),
  dict(spr=HEAD, sub=A, tics=3, act='Chase'),
  dict(spr=HEAD, sub=B, tics=3, act='Chase'),
  dict(spr=HEAD, sub=B, tics=3, act='Chase'),
  dict(spr=HEAD, sub=C, tics=3, act='Chase'),
  dict(spr=HEAD, sub=C, tics=3, act='Chase'),
  dict(spr=HEAD, sub=D, tics=3, act='Chase'),
  dict(spr=HEAD, sub=D, tics=3, act='Chase', goto=-7)
)
ranged = (
  dict(spr=HEAD, sub=E, tics=5, act='FaceTarget'),
  dict(spr=HEAD, sub=F, tics=5, act='FaceTarget'),
  dict(spr=HEAD, sub=G, tics=5, act='HeadAttack', goto='see')
)
melee = (
  dict(spr=HEAD, sub=H, tics=5, act='FaceTarget'),
  dict(spr=HEAD, sub=I, tics=5, act='FaceTarget'),
  dict(spr=HEAD, sub=J, tics=5, act='HeadAttack', goto='see')
)
injury = (
  dict(spr=HEAD, sub=K, tics=3),
  dict(spr=HEAD, sub=K, tics=3, act='Pain'),
  dict(spr=HEAD, sub=K, tics=6, goto='see')
)
death = (
  dict(spr=HEAD, sub=L, tics=8),
  dict(spr=HEAD, sub=M, tics=8, act='Scream'),
  dict(spr=HEAD, sub=N, tics=8),
  dict(spr=HEAD, sub=O, tics=8),
  dict(spr=HEAD, sub=P, tics=8),
  dict(spr=HEAD, sub=Q, tics=8),
  dict(spr=HEAD, sub=R, tics=8),
  dict(spr=HEAD, sub=S, tics=8, act='Fall'),
  dict(spr=HEAD, sub=T, tics=-1, act='Null', goto=False),
)
frames = {
  'initial': initial,
  'first moving': moving,
  'far attack': ranged,
  'close attack': melee,
  'injury': injury,
  'death': death
}
