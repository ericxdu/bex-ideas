from sprite_names import *

number = 33
name = "Bug Fireball"
initial = (
  dict(spr=BAL1, sub=A, tics=15),
  dict(spr=BAL1, sub=B, tics=15, goto=-1)
)
death = (
  dict(spr=BAL1, sub=C, tics=6),
  dict(spr=BAL1, sub=D, tics=6),
  dict(spr=BAL1, sub=E, tics=6, goto=False)
)
frames = {
  'initial': initial,
  'death': death
}
