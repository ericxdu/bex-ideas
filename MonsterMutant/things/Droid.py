from sprite_names import *

number = 23
name = "Droid"
initial = (
  dict(spr=BSPI, sub=A, tics=10, act='Look'),
  dict(spr=BSPI, sub=B, tics=10, act='Look', goto=-1)
)
moving = (
  dict(spr=BSPI, sub=A, tics=20),
  dict(spr=BSPI, sub=A, tics=3, act='Chase', tag='see'),
  dict(spr=BSPI, sub=A, tics=3, act='Chase'),
  dict(spr=BSPI, sub=B, tics=3, act='Chase'),
  dict(spr=BSPI, sub=B, tics=3, act='Chase'),
  dict(spr=BSPI, sub=C, tics=3, act='Chase'),
  dict(spr=BSPI, sub=C, tics=3, act='Chase'),
  dict(spr=BSPI, sub=D, tics=3, act='Chase'),
  dict(spr=BSPI, sub=D, tics=3, act='Chase', goto=-7)
)
attack = (
  dict(spr=BSPI, sub=E, tics=20, act='FaceTarget'),
  dict(spr=BSPI, sub=F, tics=4, act='PosAttack'),
  dict(spr=BSPI, sub=G, tics=4, act='PosAttack'),
  dict(spr=BSPI, sub=E, tics=1, act='SpidRefire', goto=-2)
)
injury = (
  dict(spr=BSPI, sub=H, tics=3),
  dict(spr=BSPI, sub=H, tics=3, act='Pain', goto='see')
)
death = (
  dict(spr=BSPI, sub=I, tics=5),
  dict(spr=BSPI, sub=J, tics=5, act='Scream'),
  dict(spr=BSPI, sub=K, tics=5),
  dict(spr=BSPI, sub=L, tics=5),
  dict(spr=BSPI, sub=M, tics=5),
  dict(spr=BSPI, sub=N, tics=5, act='Fall'),
  dict(spr=BSPI, sub=O, tics=-1, act='BossDeath', goto=False),
)
frames = {
  'initial': initial,
  'first moving': moving,
  'far attack': attack,
  'injury': injury,
  'death': death
}
