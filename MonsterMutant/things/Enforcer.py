from sprite_names import *

number = 3
name = "Enforcer"
initial = (
  dict(spr=SPOS, sub=A, tics=10, act='Look'),
  dict(spr=SPOS, sub=B, tics=10, act='Look', goto=-1)
)
moving = (
  dict(spr=SPOS, sub=A, tics=3, act='Chase', tag='see'),
  dict(spr=SPOS, sub=A, tics=3, act='Chase'),
  dict(spr=SPOS, sub=B, tics=3, act='Chase'),
  dict(spr=SPOS, sub=B, tics=3, act='Chase'),
  dict(spr=SPOS, sub=C, tics=3, act='Chase'),
  dict(spr=SPOS, sub=C, tics=3, act='Chase'),
  dict(spr=SPOS, sub=D, tics=3, act='Chase'),
  dict(spr=SPOS, sub=D, tics=3, act='Chase', goto=-7)
)
attack = (
  dict(spr=SPOS, sub=E, tics=10, act='FaceTarget'),
  dict(spr=SPOS, sub=F, tics=10, act='SPosAttack'),
  dict(spr=SPOS, sub=E, tics=10, goto='see')
)
injury = (
  dict(spr=SPOS, sub=G, tics=3),
  dict(spr=SPOS, sub=G, tics=3, act='Pain', goto='see')
)
death = (
  dict(spr=SPOS, sub=H, tics=5),
  dict(spr=SPOS, sub=I, tics=5, act='Scream'),
  dict(spr=SPOS, sub=J, tics=5, act='Fall'),
  dict(spr=SPOS, sub=K, tics=5),
  dict(spr=SPOS, sub=L, tics=5),
  dict(spr=SPOS, sub=M, tics=-1, goto=False),
)
frames = {
  'initial': initial,
  'first moving': moving,
  'far attack': attack,
  'injury': injury,
  'death': death
}
