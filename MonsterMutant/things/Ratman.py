from sprite_names import *

number = 11
name = "Ratman"
initial = (
  dict(spr=CPOS, sub=A, tics=10, act='Look'),
  dict(spr=CPOS, sub=B, tics=10, act='Look', goto=-1)
)
moving = (
  dict(spr=CPOS, sub=A, tics=3, act='Chase', tag='see'),
  dict(spr=CPOS, sub=A, tics=3, act='Chase'),
  dict(spr=CPOS, sub=B, tics=3, act='Chase'),
  dict(spr=CPOS, sub=B, tics=3, act='Chase'),
  dict(spr=CPOS, sub=C, tics=3, act='Chase'),
  dict(spr=CPOS, sub=C, tics=3, act='Chase'),
  dict(spr=CPOS, sub=D, tics=3, act='Chase'),
  dict(spr=CPOS, sub=D, tics=3, act='Chase', goto=-7)
)
attack = (
  dict(spr=CPOS, sub=E, tics=10, act='FaceTarget'),
  dict(spr=CPOS, sub=E, tics=2, act='FaceTarget'),
  dict(spr=CPOS, sub=F, tics=4, act='CPosAttack'),
  dict(spr=CPOS, sub=E, tics=1, act='CPosRefire', goto=-2)
)
"""
attack = (
  dict(spr=CPOS, sub=E, tics=4, act='CPosAttack'),
  dict(spr=CPOS, sub=F, tics=4, act='CPosAttack'),
  dict(spr=CPOS, sub=E, tics=4, act='CPosAttack'),
  dict(spr=CPOS, sub=F, tics=4, act='CPosAttack'),
  dict(spr=CPOS, sub=E, tics=4, act='CPosAttack'),
  dict(spr=CPOS, sub=F, tics=4, act='CPosAttack', goto='see')
)
"""
injury = (
  dict(spr=CPOS, sub=G, tics=3),
  dict(spr=CPOS, sub=G, tics=3, act='Pain', goto='see')
)
death = (
  dict(spr=CPOS, sub=H, tics=5),
  dict(spr=CPOS, sub=I, tics=5, act='Scream'),
  dict(spr=CPOS, sub=J, tics=5, act='Fall'),
  dict(spr=CPOS, sub=K, tics=5),
  dict(spr=CPOS, sub=L, tics=5),
  dict(spr=CPOS, sub=M, tics=-1, goto=False),
)
frames = {
  'initial': initial,
  'first moving': moving,
  'far attack': attack,
  'injury': injury,
  'death': death
}
