from sprite_names import *

number = 2
name = "Stability Officer"
initial = (
  dict(spr=SSWV, sub=A, tics=10, act='Look'),
  dict(spr=SSWV, sub=B, tics=10, act='Look', goto=-1)
)
moving = (
  dict(spr=SSWV, sub=A, tics=3, act='Chase', tag='see'),
  dict(spr=SSWV, sub=A, tics=3, act='Chase'),
  dict(spr=SSWV, sub=B, tics=3, act='Chase'),
  dict(spr=SSWV, sub=B, tics=3, act='Chase'),
  dict(spr=SSWV, sub=C, tics=3, act='Chase'),
  dict(spr=SSWV, sub=C, tics=3, act='Chase'),
  dict(spr=SSWV, sub=D, tics=3, act='Chase'),
  dict(spr=SSWV, sub=D, tics=3, act='Chase', goto=-7)
)
attack = (
  dict(spr=SSWV, sub=E, tics=10, act='FaceTarget'),
  dict(spr=SSWV, sub=F, tics=10, act='FaceTarget'),
  dict(spr=SSWV, sub=G, tics=8, act='PosAttack', goto='see')
)
injury = (
  dict(spr=SSWV, sub=H, tics=3),
  dict(spr=SSWV, sub=H, tics=3, act='Pain', goto='see')
)
death = (
  dict(spr=SSWV, sub=I, tics=5),
  dict(spr=SSWV, sub=J, tics=5, act='Scream'),
  dict(spr=SSWV, sub=K, tics=5, act='Fall'),
  dict(spr=SSWV, sub=L, tics=5),
  dict(spr=SSWV, sub=M, tics=5),
  dict(spr=SSWV, sub=N, tics=5),
  dict(spr=SSWV, sub=O, tics=-1, goto=False),
)
frames = {
  'initial': initial,
  'first moving': moving,
  'far attack': attack,
  'injury': injury,
  'death': death
}
