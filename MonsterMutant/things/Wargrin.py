from sprite_names import *

number = 12
name = "Wargrin"
initial = (
  dict(spr=CYBR, sub=A, tics=15, act='Look'),
  dict(spr=CYBR, sub=B, tics=15, act='Look', goto=-1)
)
moving = (
  dict(spr=CYBR, sub=A, tics=3, act='Chase', tag='see'),
  dict(spr=CYBR, sub=A, tics=3, act='Chase'),
  dict(spr=CYBR, sub=B, tics=3, act='Chase'),
  dict(spr=CYBR, sub=B, tics=3, act='Chase'),
  dict(spr=CYBR, sub=C, tics=3, act='Chase'),
  dict(spr=CYBR, sub=C, tics=3, act='Chase'),
  dict(spr=CYBR, sub=D, tics=3, act='Chase'),
  dict(spr=CYBR, sub=D, tics=3, act='Chase', goto=-7)
)
ranged = (
  dict(spr=CYBR, sub=E, tics=8, act='FaceTarget'),
  dict(spr=CYBR, sub=F, tics=8, act='FaceTarget'),
  dict(spr=CYBR, sub=G, tics=6, act='TroopAttack', goto='see')
)
melee = (
  dict(spr=CYBR, sub=H, tics=8, act='FaceTarget'),
  dict(spr=CYBR, sub=I, tics=8, act='FaceTarget'),
  dict(spr=CYBR, sub=J, tics=6, act='TroopAttack', goto='see')
)
injury = (
  dict(spr=CYBR, sub=K, tics=2),
  dict(spr=CYBR, sub=K, tics=2, act='Pain', goto='see')
)
death = (
  dict(spr=CYBR, sub=L, tics=8),
  dict(spr=CYBR, sub=M, tics=8, act='Scream'),
  dict(spr=CYBR, sub=N, tics=6),
  dict(spr=CYBR, sub=O, tics=6, act='Fall'),
  dict(spr=CYBR, sub=P, tics=-1, goto='see')
)
frames = {
  'initial': initial,
  'first moving': moving,
  'far attack': ranged,
  'close attack': melee,
  'injury': injury,
  'death': death
}
