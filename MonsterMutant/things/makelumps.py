import WargrinFireball
import Wargrin
import Droid
import BugFireball
import Bug
import StabilityOfficer
import Enforcer
import Ratman

DEHACKED_FILENAME = "dehacked.lmp"
CODEPTRS_FILENAME = "codeptrs.lmp"
THINGDEF_FILENAME = "thingdefs.lmp"
CCSTRING_FILENAME = "ccstrings.lmp"

def write_state(n, x, state, thing_name, state_name, tags):
  dehfile = open(DEHACKED_FILENAME, "a")
  ptrfile = open(CODEPTRS_FILENAME, "a")
  dehfile.write(
      "Frame " + str(n)
      + " ("
      + thing_name + " "
      + state_name + " "
      + str(x + 1)
      + ")\n"
  )
  dehfile.write("sprite number = " + str(state['spr']) + "\n")
  dehfile.write("sprite subnumber = " + str(state['sub']) + "\n")
  dehfile.write("duration = " + str(state['tics']) + "\n")
  if not 'goto' in state:
    dehfile.write("next frame = " + str(n + 1) + "\n")
  elif not state['goto']:
    dehfile.write("next frame = 0\n")
  elif not isinstance(state['goto'], int):
    dehfile.write("next frame = " + str(tags[state[ 'goto' ]]) + "\n")
  else:
    dehfile.write("next frame = " + str(n + state[ 'goto' ]) + "\n")
  if not 'act' in state:
    ptrfile.write("frame " + str(n) + " = Null")
  else:
    dehfile.write("#action pointer = " + state['act'] + "\n")
    ptrfile.write("frame " + str(n) + " = " + state['act'])
  dehfile.write("\n")
  ptrfile.write("  #" + thing_name + " " + state_name + " " + str(x + 1))
  ptrfile.write("\n")
  dehfile.close()
  ptrfile.close()

with open(DEHACKED_FILENAME, 'w') as file:
  file.write("Patch File for DeHackEd v3.0\n")
  file.write("\n")
  file.write("# Note: Use the pound sign ('#') to start comment lines.\n")
  file.write("\n")
  file.write("Doom version = 19\n")
  file.write("Patch format = 6\n")
  file.write("\n")
with open(CODEPTRS_FILENAME, 'w') as file:
  file.write("[codeptr]\n")
with open(THINGDEF_FILENAME, 'w') as file:
  file.write("")
with open(CCSTRING_FILENAME, 'w') as file:
  file.write("\n")
  file.write("[strings]\n")
  
def make_thing(n, module):
  strings = {
    1: "CC_HERO",
    2: "CC_ZOMBIE",
    3: "CC_SHOTGUN",
    4: "CC_VILE",
    6: "CC_REVEN",
    9: "CC_MANCU",
    11: "CC_HEAVY",
    12: "CC_IMP",
    13: "CC_DEMON",
    15: "CC_CACO",
    16: "CC_BARON",
    18: "CC_HELL",
    19: "CC_LOST",
    20: "CC_SPIDER",
    21: "CC_ARACH",
    22: "CC_CYBER",
    23: "CC_PAIN"}
  if module.number in strings:
    strfile = open(CCSTRING_FILENAME, 'a')
    strfile.write(strings[module.number] + " = " + module.name + "\n")
  file = open(THINGDEF_FILENAME, 'a')
  file.write("Thing " + str(module.number) + " (" + module.name + ")")
  file.write("\n")
  tags = {}
  frames = {
    "initial": "initial",
    "first moving": "moving",
    "injury": "injury",
    "close attack": "melee",
    "far attack": "ranged",
    "death": "death",
    "exploding": "exploding",
    "respawn": "respawn"}
  for name in frames:
    if name in module.frames:
      file.write(name + " frame = " + str(n) + "\n")
      for x, state in enumerate(module.frames[name]):
        if 'tag' in state:
          tags[state['tag']] = n
        write_state(n, x, state, module.name, frames[name], tags)
        n = n + 1
    else:
      file.write(name + " frame = 0\n")
  file.write("\n")
  return n
n = 174
n = make_thing(n, WargrinFireball)
n = make_thing(n, Wargrin)
n = make_thing(n, BugFireball)
n = make_thing(n, Bug)
n = make_thing(n, StabilityOfficer)
n = make_thing(n, Enforcer)
n = make_thing(n, Ratman)
n = make_thing(n, Droid)
