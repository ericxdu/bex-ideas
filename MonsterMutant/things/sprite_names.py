#sprite numbers
SHTG = 1
PUNG = 2
PISG = 3
PISF = 4
SHTF = 5
SHT2 = 6
CHGG = 7
CHGF = 8
MISG = 9
MISF = 10
SAWG = 11
PLSG = 12
PLSF = 13
BFGG = 14
BFGF = 15
BLUD = 16
PUFF = 17
BAL1 = 18
BAL2 = 19
PLSS = 20
PLSE = 21
MISL = 22
BFS1 = 23
BFE1 = 24
BFE2 = 25
TFOG = 26
IFOG = 27
PLAY = 28
POSS = 29
SPOS = 30
VILE = 31
FIRE = 32
FATB = 33
FBXP = 34
SKEL = 35
MANF = 36
FATT = 37
CPOS = 38
SARG = 39
HEAD = 40
BAL7 = 41
BOSS = 42
BOS2 = 43
SKUL = 44
SPID = 45
BSPI = 46
APLS = 47
APBX = 48
CYBR = 49
PAIN = 50
SSWV = 51
KEEN = 52
BBRN = 53
BOSF = 54
ARM1 = 55
ARM2 = 56
BAR1 = 57
BEXP = 58
FCAN = 59
BON1 = 60
BON2 = 61
BKEY = 62
RKEY = 63
YKEY = 64
BSKU = 65
RSKU = 66
YSKU = 67
STIM = 68
MEDI = 69
SOUL = 70
PINV = 71
PSTR = 72
PINS = 73
MEGA = 74
SUIT = 75
PMAP = 76
PVIS = 77
CLIP = 78
AMMO = 79
ROCK = 80
BROK = 81
CELL = 82
CELP = 83
SHEL = 84
SBOX = 85
BPAK = 86
BFUG = 87
MGUN = 88
CSAW = 89
LAUN = 90
PLAS = 91
SHOT = 92
SGN2 = 93
COLU = 94
SMT2 = 95
GOR1 = 96
POL2 = 97
POL5 = 98
POL4 = 99
POL3 = 100
POL1 = 101
POL6 = 102
GOR2 = 103
GOR3 = 104
GOR4 = 105
GOR5 = 106
SMIT = 107
COL1 = 108
COL2 = 109
COL3 = 110
COL4 = 111
CAND = 112
CBRA = 113
COL6 = 114
TRE1 = 115
TRE2 = 116
ELEC = 117
CEYE = 118
FSKU = 119
COL5 = 120
TBLU = 121
TGRN = 122
TRED = 123
SMBT = 124
SMGT = 125
SMRT = 126
HDB1 = 127
HDB2 = 128
HDB3 = 129
HDB4 = 130
HDB5 = 131
HDB6 = 132
POB1 = 133
POB2 = 134
BRS1 = 135
TLMP = 136
TLP2 = 137
#subsprite numbers
A = 0
B = 1
C = 2
D = 3
E = 4
F = 5
G = 6
H = 7
I = 8
J = 9
K = 10
L = 11
M = 12
N = 13
O = 14
P = 15
Q = 16
R = 17
S = 18
T = 19
U = 20
V = 21
W = 22
X = 23
Y = 24
Z = 25
