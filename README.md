## Building

To make a PWAD from any subproject, enter its directory and use DeuTex.

  > deutex -make pwadname.wad

## Running

Run with your Boom-compatible source port.

  > boom -file pwadname.wad

To use with a specific IWAD, use the -iwad tag. To combine with another 
PWAD, add it to the -file tag.

  > boom -iwad freedoom2.wad -file megawad.wad pwadname.wad
