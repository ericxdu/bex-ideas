#!/usr/bin/env python3
from __future__ import print_function

import sys

from deh9000 import *

miscdata.initial_bullets = 0
miscdata.bfg_cells_per_shot = 1

mobjinfo[MT_SERGEANT].spawnhealth = 20
mobjinfo[MT_SERGEANT].mass = 5

mobjinfo[MT_MISC2].clear_states()
dehfile.reclaim_states(4)
with open("coin.states") as file:
    mobjinfo[MT_MISC2].update(states.parse(file.read( )))

def ruby(mt, spr):
    mobjinfo[mt].clear_states()
    dehfile.reclaim_states(7)
    with open("ruby.states") as file:
        mobjinfo[mt].update(
	    states.parse(file.read( ).replace("RUBY", spr)))

ruby(MT_MISC4, "BKEY")
ruby(MT_MISC5, "RKEY")
ruby(MT_MISC6, "YKEY")
ruby(MT_MISC7, "YSKU")
ruby(MT_MISC8, "RSKU")
ruby(MT_MISC9, "BSKU")

strings["BON1"] = "COIN"
strings["PISTOL"] = "GLDHIT"
strings["PUFF"] = "PUF2"
strings["RUNNIN"] = "FUNISI"

dehfile.save(sys.argv[1])

state_ids = dehfile.free_states()
if state_ids:
    print("Still have %d states remaining" % len(state_ids))
