# Levels

Each level should be a sandbox of sorts where the player is expected
to overcome challenges and puzzles to collect a number of crystal
skulls.

1. green fields
2. brick fortress
3. underwater bay
4. snowy mountain
5. haunted house
6. misty cave
7. lava lake
8. windy desert
9. submarine dock
10. snowy fields
11. underwater city
12. windy mountain
13. tropical island
14. clockwork tower
15. cloudy sky

1. red brick roads
  + lots of stairs/stairbuilders
2. blue brick caves
  + lots of lifts and pits
3. grey stone forts
  + lakes of lava
  + boss monsters
4. clear blue lakes

# OLD

This is a Boom mod with a touch of whimsy. You play a kind of treasure
hunter who can use fireballs and cannonballs to pester fellow hunters.
You must avoid dangerous traps and solve devious puzzles to find the
key and reach the treasure on each map.

## Ideas

Warp Rings: metal-lipped circles with a teleport pad on the inside.

Bug Burrows: shallow pits that look suspiciously like warp rings hide a
dangerous flying creature (HEAD) that will surprise and attack 
approaching hunters.

Stomp Switches: raised poles or buttons that the hunter must depress 
into the ground in order to achieve an objective.

Spike Pits: areas of lower ground with spikes that harm the hunter on 
contact.

Fireballs: the hunter's main weapon is a supply of magic fireballs 
thrown from their fist.

Porta-cannon: a slow-firing weapon that launches explosive cannonballs.

Golden Gun: a scattershot weapon that throws coins at high velocity at 
enemies.

Rapidaxe: magic axes that are rapid-fire thrown at mid-range enemies.

Shade: floating, invisible enemy with melee attack.

OneOne: the first level, open design, concepts from SMB:1-1 and SM64:BOB

House: new level concept, haunted house design, tricky to find exit
