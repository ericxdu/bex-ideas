[flats]
flat1
tile092
tile105
tile115
tile124

[levels]
map01

[lumps]
dehacked

[musics]
d_funisi

[patches]
rsky1

[sounds]
DSBFG
DSGETPOW
DSITEMUP
DSPLASMA
DSTELEPT
DSWPNUP
dsgldhit

[sprites]
bkeya0	12	48
bkeyb0	12	48
bkeyc0	12	48
bkeyd0	12	48
bkeye0	12	48
bkeyf0	12	48
bkeyg0	12	48
bskua0	12	48
bskub0	12	48
bskuc0	12	48
bskud0	12	48
bskue0	12	48
bskuf0	12	48
bskug0	12	48
coina0  8	32
coinb0	8	32
coinc0	8	32
coind0	8	32
rkeya0	12	48
rkeyb0	12	48
rkeyc0	12	48
rkeyd0	12	48
rkeye0	12	48
rkeyf0	12	48
rkeyg0	12	48
rskua0	12	48
rskub0	12	48
rskuc0	12	48
rskud0	12	48
rskue0	12	48
rskuf0	12	48
rskug0	12	48
pisga0	-128	-64
pisgb0	-128	-64
pisgc0	-128	-64
plssa0
plssb0
plssc0
plssd0
PUNGA0  -100	-138
PUNGB0  -68	-75
PUNGC0  -273	-76
PUNGD0  -200	-67
PUF2A0   15 15   # Stilgar, recolored by BethOfDeath aka bpeterson
PUF2B0   14 14 # Stilgar, recolored by BethOfDeath aka bpeterson
PUF2C0   14 14 # Stilgar, recolored by BethOfDeath aka bpeterson
PUF2D0   14 15 # Stilgar, recolored by BethOfDeath aka bpeterson
PUF2E0   15 16 # Stilgar, recolored by BethOfDeath aka bpeterson
ykeya0	12	48
ykeyb0	12	48
ykeyc0	12	48
ykeyd0	12	48
ykeye0	12	48
ykeyf0	12	48
ykeyg0	12	48
yskua0	12	48
yskub0	12	48
yskuc0	12	48
yskud0	12	48
yskue0	12	48
yskuf0	12	48
yskug0	12	48

[texture1]
texture1
