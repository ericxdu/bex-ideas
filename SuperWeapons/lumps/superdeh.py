#!/usr/bin/env python3
from __future__ import print_function

import sys

from deh9000 import *

dehfile.reclaim_states(6)

ammodata[am_clip].clipammo = 15
ammodata[am_clip].maxammo = 300

mobjinfo[MT_SERGEANT].clear_states()
mobjinfo[MT_SHADOWS].clear_states()
mobjinfo[MT_MISC64].clear_states()
with open("superdemon.states") as file:
    mobjinfo[MT_SERGEANT].update(states.parse(file.read( )))
    mobjinfo[MT_SHADOWS].copy_from(mobjinfo[MT_SERGEANT])

mobjinfo[MT_FATSO].spawnhealth = 900
mobjinfo[MT_FATSO].painchance = 80
mobjinfo[MT_FATSO].clear_states()
with open("supermancubus.states") as file:
    mobjinfo[MT_FATSO].update(states.parse(file.read( )))

weaponinfo[wp_pistol].clear_states()
with open("superpistol.states") as file:
    weaponinfo[wp_pistol].update(states.parse(file.read( )))

weaponinfo[wp_chaingun].clear_states()
with open("superchaingun.states") as file:
    weaponinfo[wp_chaingun].update(states.parse(file.read( )))

dehfile.save(sys.argv[1])

state_ids = dehfile.free_states()
if state_ids:
    print("Still have %d states remaining" % len(state_ids))
