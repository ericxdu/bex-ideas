Super Weapons
=============

Inspired by the Super Shotgun, this modification introduces the Super 
Pistol and Super Chaingun designed by Skillsaw with skins by Railgunner 
and Espi. Also included are upgraded enemies and better drops to match
the power and ammo consumption of the new weapons.


New Assets
----------

Super Bullets (Ammo 0)

Arachnorb / Grell (Thing 139)

Super Mancubus / Harubus (Thing 9)

Super Demon / Hindring (Thing 13)

Super Pistol (Weapon 1)

Super Chaingun (Weapon 4)
