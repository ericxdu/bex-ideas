#!/usr/bin/env python3
from __future__ import print_function

import sys

from deh9000 import *

ammodata[am_clip].maxammo = 999
ammodata[am_clip].clipammo = 10

miscdata.initial_bullets = 100

mobjinfo[MT_PLASMA].damage = 3
mobjinfo[MT_PLASMA].deathsound = 0
mobjinfo[MT_PLASMA].clear_states()
mobjinfo[MT_PLASMA].update(states.parse("""
    Spawn:
        BAL2 A 2
        BAL2 B 2
        Loop
    Death:
        PUFF A 5
        PUFF B 5
        PUFF C 5
        PUFF D 5
        Stop
"""))

mobjinfo[MT_BOSSBRAIN].clear_states()
with open('decorate/BossBrain', 'r') as file:
    mobjinfo[MT_BOSSBRAIN].update(states.parse(file.read()))
weaponinfo[wp_fist].clear_states()
weaponinfo[wp_pistol].clear_states()
weaponinfo[wp_shotgun].clear_states()
weaponinfo[wp_chaingun].clear_states()
weaponinfo[wp_missile].clear_states()
weaponinfo[wp_plasma].clear_states()
weaponinfo[wp_bfg].clear_states()
weaponinfo[wp_fist].update(states.parse("""
    Ready:
        PISG A 1 A_WeaponReady
        Loop
    Deselect:
        PISG A 1 A_Lower
        Loop
    Select:
        PISG A 1 A_Raise
        Loop
    Fire:
        PISG G 3
        PISG H 3 A_Punch
        PISG I 4
        PISG J 1
        Goto Ready
"""))
with open('decorate/Pistol', 'rt') as file:
    weaponinfo[wp_pistol].update(states.parse(file.read( )))
with open('decorate/Chaingun', 'rt') as file:
    weaponinfo[wp_chaingun].update(states.parse(file.read( )))

#0 Fist
#1 Pistol
weaponinfo[wp_shotgun].copy_from(weaponinfo[wp_pistol])  #2 Shotgun
weaponinfo[wp_chaingun].copy_from(weaponinfo[wp_chaingun])  #3 Chaingun
weaponinfo[wp_missile].copy_from(weaponinfo[wp_pistol])  #4 Missile
weaponinfo[wp_plasma].copy_from(weaponinfo[wp_pistol])  #5 Plasma
weaponinfo[wp_bfg].copy_from(weaponinfo[wp_pistol])  #6 BFG
weaponinfo[wp_chainsaw].copy_from(weaponinfo[wp_pistol])  #7 Chainsaw
weaponinfo[wp_supershotgun].copy_from(weaponinfo[wp_pistol])  #8 Super Shotgun

states[S_BAR1].tics = -1
states[S_BAR1].nextstate = S_NULL

strings["BAR1"] = "BARR"
strings["BBRN"] = "PROP"
strings["PLSS"] = "BAL3"

# Sounds
strings["popain"] = "hurt"
strings["getpow"] = "health"
strings["itemup"] = "key"
strings["plasma"] = "player"
strings["pstop"] = "cursor"
strings["wpnup"] = "ammo"

# Doom2 exit sounds
strings["vilact"] = "ammo"
strings["getpow"] = "health"
strings["boscub"] = "door"
strings["slop"] = "explod"
strings["skeswg"] = "cursor"
strings["kntdth"] = "hurt"
strings["bspact"] = "enemy"
strings["sgtatk"] = "player"

# Music tracks
strings["dm2ttl"] = "title"
strings["dm2int"] = "inter"
strings["runnin"] = "lev41"

dehfile.save(sys.argv[1])

state_ids = dehfile.free_states()
if state_ids:
    print("Still have %d states remaining" % len(state_ids))
