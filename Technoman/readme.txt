# Technoman 3D

A minimalist Doom mod in Megaman style.


Level Design
------------

Similar to Wolfenstein 3D.


STARMS, STGNUM and STYSNUM
--------------------------

These features of the statusbar were removed (made blank) similar to the way Chex Quest does so for aesthetic reasons. This technique is compatible with vanilla and Chocolate Doom. The corresponding areas of the statusbar should be replaced by cool-looking panels.
