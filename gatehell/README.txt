Preserving some old Freedoom assets in a playable state, starting with
FreeDM map11 "Gate For Hell" which was removed in 0.12.1, and "MAP01"
which was moved or removed somewhere around version 0.9.

"Gate For Hell" along with its music has been slotted into map01 as
well as map11, until my restoration of the original map01 is complete.

I've curated the weapon and pickup sprites to fit the sort of "desert
ruins" or "adventurous archeologist" aesthetic of "Gate for Hell".

# Issues

In order to make this wad work with DOOM II I had to provide a TEXTURE2
lump. However, this seems to make it incompatible with some versions of
Freedoom 2. This will need to be remedied for the wad to be cross-
compatible with all intended IWADs.

If you want this to work with DOOM II, you'll have to uncomment the
TEXTURE2 line in "wadinfo.txt" before building with DeuTEX.
