I'm using textures from the texture pack [FPS Textures, flat and more](https://opengameart.org/content/fps-textures-flat-and-more) to construct a minimal IWAD.

I noticed some of the subtle blues get crunched down to grays by DeuTeX, so I've attempted to modify the standard doom palette with a grandient that mostly matches at key points with the textures. Leaving alone the texture palettes themselves as they convert readily to the standard doom palette, albeit with color loss.
